(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["partner-account-guide-partner-account-guide-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/partner-account-guide/partner-account-guide.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/partner-account-guide/partner-account-guide.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<div class=\"slide\">\r\n    <h1>MINDSET APP - ACCOUNT VERIFICATION\r\n        <span class=\"subHeading\">HOW PARTNERS GET STARTED ON THE APP</span>\r\n    </h1>\r\n</div>\r\n<section class=\"bg-white\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-12 py-5\">\r\n               <p>\r\n                    <strong>FOR SPEAKERS AND CREATORS</strong><br>\r\n \r\n                    <strong>Create, Claim and Verify your Partner Account.</strong><br><br>\r\n                    Want to learn how to start or verify your Mindset App partner account? It’s easy. Follow these steps to 1) create or claim your account 2) verify your account, and 3) share your content for growth. Get started and earning in no time!<br><br>\r\n                    <strong>STEP 1: Create or Claim your partner account</strong> (if you already have an account, go to step 2)<br>\r\n                    To start your partner account on Mindset App, click <a href=\"https://docs.google.com/forms/d/e/1FAIpQLSe7yP3eMIqQNYJiTSxCIccr2ft9mzihWyYgEgqp-krKYnQryA/viewform?usp=sf_link\" target=\"_blank\" class=\"theme-color-blue\">here</a> to apply.<br><br>\r\n                         To claim your auto-initiated Mindset App account, click <a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdvUydziJIzbpaDKq4gxxgQw_lRv_0E_tG1mucr6dChw7ZsOA/viewform?usp=sf_link\" target=\"_blank\" class=\"theme-color-blue\">here</a>.*<br><br>\r\n                    * You may notice you already have an auto-initiated account on Mindset App. This is because all content on the App require proper speaker and creator credits. As a result, if your name was credited as a content contributor, your account was auto-initiated. This is good news! It means your account has had a head-start! It also means it’s time to <a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdvUydziJIzbpaDKq4gxxgQw_lRv_0E_tG1mucr6dChw7ZsOA/viewform?usp=sf_link\" target=\"_blank\" class=\"theme-color-blue\">claim</a> your account, which is the first step to start earning through the App.<br><br>\r\n                    \r\n                    Of course, to claim, we’ll ask you to confirm you’re the rightful owner of that account (easily done by sending a direct message through your official Facebook, Twitter or Instagram page, or by attaching an image with ID that proves your identity or brand ownership).<br><br>\r\n                    <strong>STEP 2: Verify your partner account to start earning</strong><br>\r\n                    VERIFY: Applying to verify your partner account is easy. Start by clicking <a href=\"https://docs.google.com/forms/d/e/1FAIpQLScha0Ihk6lmZNU-LPEwJL3fH7Vv3bMjPJBL7YUl7YNiRqIOew/viewform?usp=sf_link\" target=\"_blank\" class=\"theme-color-blue\">here</a>.<br>\r\n                    Once you have been verified, you’ll notice a blue checkmark beside your account profile image. This checkmark shows users you’re a Verified Mindset Partner and it also means you will earn through our PartnerShare program when your content get played. Once you’re approved, expect to receive an email from our team requesting your PayPal payment details*. This allows us to start sending your earnings every month**.<br><br>\r\n                      \r\n                    <strong>*Partner Manager</strong> – In the approval email we will send you, contact information for your Mindset App partner manager will be included. This is the person you can contact with future questions.<br><br>\r\n                     \r\n                    **Your revenue accrues every month on Mindset App but we do not pay out through our PartnerShare program until you earn more that $100 USD. For months where your total accumulated revenue is less than $100 USD, your revenue will roll over and add to your next month’s total.<br><br>\r\n                    \r\n                    <strong>STEP 3: Share through our Content Database to amplify your growth</strong><br>\r\n                          Click <span routerLink=\"/contact-us\" class=\"theme-color-blue pointer\">here</span> to sign up and learn more about our exclusive database.<br>\r\n                    Now that you have a verified partner account on Mindset App, you will be able to start sharing your content to amplify your earnings through the App. Here’s how: Mindset App is launching an exclusive database of permission granted content that Creators can pull from to create new audio tracks. By loading your content into that database, you will be increasing your chances to have more tracks on the App, and as a result, earn more.<br><br>\r\n                    You choose the content you will share and you’re the one to give permission (so you’re in control) and set requirements, but by sharing through the database, you will be helping your content reach more people, and in some cases 10x as many people. It’s the fastest way to grow your earnings on Mindset App.\r\n                    \r\n               </p>\r\n                <!-- <button class=\"btn theme-color-blue\"><i class=\"fas fa-long-arrow-alt-left mr-2\" (ngClick)=\"goToUrl()\"></i>Back</button> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</section>\r\n<section class=\"bg-black pt-4 pb-1\">\r\n    <app-footer></app-footer>\r\n</section>"

/***/ }),

/***/ "./src/app/partner-account-guide/partner-account-guide-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/partner-account-guide/partner-account-guide-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: PartnerAccountGuideRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnerAccountGuideRoutingModule", function() { return PartnerAccountGuideRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _partner_account_guide_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partner-account-guide.component */ "./src/app/partner-account-guide/partner-account-guide.component.ts");




const routes = [{ path: '', component: _partner_account_guide_component__WEBPACK_IMPORTED_MODULE_3__["PartnerAccountGuideComponent"] }];
let PartnerAccountGuideRoutingModule = class PartnerAccountGuideRoutingModule {
};
PartnerAccountGuideRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PartnerAccountGuideRoutingModule);



/***/ }),

/***/ "./src/app/partner-account-guide/partner-account-guide.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/partner-account-guide/partner-account-guide.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pointer {\n  cursor: pointer;\n}\n\n* {\n  outline: none !important;\n}\n\n.fw-100 {\n  font-weight: 100;\n}\n\n.fw-400 {\n  font-weight: 400;\n}\n\n.theme-color-blue {\n  color: #0462EE;\n}\n\n.bg-black {\n  background-color: #000000 !important;\n}\n\ninput {\n  border-color: #ffffff !important;\n}\n\n.input-group-text {\n  color: #ffffff !important;\n  background-color: #2F2F2F;\n  border-color: #2F2F2F;\n}\n\n.mw-150px {\n  max-width: 200px;\n}\n\n@media (min-width: 992px) {\n  .input-group-lg-50 {\n    max-width: 35%;\n  }\n}\n\n.btn:focus, .btn.focus {\n  box-shadow: none !important;\n}\n\n.btn-p1 {\n  padding: 11px 20px;\n}\n\n.btn-light {\n  color: #ffffff !important;\n  background-color: rgba(194, 195, 197, 0.3) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: auto;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n  padding: 0px 30px;\n}\n\n.btn-light:hover, .btn-light:focus, .btn-light:active {\n  background-color: rgba(248, 249, 250, 0.5) !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-white {\n  color: #0462EE;\n  background-color: rgba(248, 249, 250, 0.9) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: 180px;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n}\n\n.btn-white:hover, .btn-white:focus, .btn-white:active {\n  background-color: #ffffff !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-responsive {\n  width: auto;\n}\n\n@media (min-width: 992px) {\n  .btn-responsive {\n    padding: 0px 50px;\n  }\n}\n\nbody {\n  background-color: #0462EE;\n}\n\n@media (max-width: 993px) {\n  h1 {\n    font-size: 2rem;\n  }\n}\n\n.slide {\n  background: url('terms-header.png') no-repeat;\n  background-size: cover;\n  background-position: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFydG5lci1hY2NvdW50LWd1aWRlL0Q6XFxhbmd1bGFyXFxtaW5kc2V0X3dlYi9zcmNcXGFwcFxccGFydG5lci1hY2NvdW50LWd1aWRlXFxwYXJ0bmVyLWFjY291bnQtZ3VpZGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhcnRuZXItYWNjb3VudC1ndWlkZS9wYXJ0bmVyLWFjY291bnQtZ3VpZGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhcnRuZXItYWNjb3VudC1ndWlkZS9EOlxcYW5ndWxhclxcbWluZHNldF93ZWIvc3RkaW4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBUyxlQUFBO0FDRVQ7O0FEREE7RUFBRSx3QkFBQTtBQ0tGOztBRGtCQTtFQUFpQixnQkFBQTtBQ2RqQjs7QURlQTtFQUFRLGdCQUFBO0FDWFI7O0FEMEJBO0VBQWtCLGNBRkE7QUNwQmxCOztBRHVCQTtFQUFXLG9DQUxFO0FDZGI7O0FEcUJBO0VBQU0sZ0NBWE87QUNOYjs7QURrQkE7RUFBa0IseUJBWkw7RUFZeUIseUJBUG5CO0VBT3dELHFCQVB4RDtBQ0xuQjs7QURjQTtFQUFVLGdCQUFBO0FDVlY7O0FEWUk7RUFESjtJQUVRLGNBQUE7RUNSTjtBQUNGOztBRFdBO0VBQXVCLDJCQUFBO0FDUHZCOztBRFFBO0VBQVEsa0JBQUE7QUNKUjs7QURLQTtFQUFZLHlCQXZCQztFQXVCbUIscURBdEJqQjtFQXNCa0QsaURBdEJsRDtFQXVCZixXQUFBO0VBQVkscUJBQUE7RUFBcUIsZ0JBQUE7RUFBaUIsWUFBQTtFQUFhLGlCQUFBO0FDSy9EOztBREpJO0VBQTBCLHFEQXZCZjtFQXVCaUQsaURBdkJqRDtBQytCZjs7QUROQTtFQUFZLGNBckJNO0VBcUJtQixxREF4QnRCO0VBd0J1RCxpREExQnZEO0VBMkJYLFlBQUE7RUFBYSxxQkFBQTtFQUFxQixnQkFBQTtFQUFpQixZQUFBO0FDZXZEOztBRGRRO0VBQTBCLG9DQTdCckI7RUE2QnFELGlEQTNCbkQ7QUM2Q2Y7O0FEaEJBO0VBQ0ksV0FBQTtBQ21CSjs7QURsQkk7RUFGSjtJQUdRLGlCQUFBO0VDcUJOO0FBQ0Y7O0FEaEJBO0VBQU0seUJBbENZO0FDc0RsQjs7QURuQkE7RUFDSTtJQUFHLGVBQUE7RUN1Qkw7QUFDRjs7QUNoR0E7RUFDSSw2Q0FBQTtFQUFrRSxzQkFBQTtFQUF1QiwyQkFBQTtBRG9HN0YiLCJmaWxlIjoic3JjL2FwcC9wYXJ0bmVyLWFjY291bnQtZ3VpZGUvcGFydG5lci1hY2NvdW50LWd1aWRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBvaW50ZXJ7Y3Vyc29yOiBwb2ludGVyO31cclxuKntvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7fVxyXG5cclxuXHJcbi8vICB0cmFuc2l0aW9uXHJcbiV0cmFuc2l0aW9uey13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLW91dDstbW96LXRyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1vdXQ7LW8tdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLW91dDt0cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2Utb3V0O31cclxuJXRyYW5zaXRpb24xey13ZWJraXQtdHJhbnNpdGlvbjogLjVzIGVhc2UtaW4tb3V0Oy1tb3otdHJhbnNpdGlvbjogLjVzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IC41cyBlYXNlLWluLW91dDt0cmFuc2l0aW9uOiAuNXMgZWFzZS1pbi1vdXQ7fVxyXG4ldHJhbnNpdGlvbjJ7LXdlYmtpdC10cmFuc2l0aW9uOiB3aWR0aCAycyBlYXNlLWluLW91dDstbW96LXRyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0O3RyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0O31cclxuJXRyYW5zaXRpb24zey13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDJzIGVhc2UtaW4tb3V0Oy1tb3otdHJhbnNpdGlvbjogYWxsIDJzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IGFsbCAycyBlYXNlLWluLW91dDt0cmFuc2l0aW9uOiBhbGwgMnMgZWFzZS1pbi1vdXQ7fVxyXG5cclxuLy9ncmFkaWFudFxyXG4lYmxhY2stYmd7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgYmFja2dyb3VuZDogLW1vei1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIGZmMy42KyAqL1xyXG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1ncmFkaWVudChyYWRpYWwsIGNlbnRlciBjZW50ZXIsIDBweCwgY2VudGVyIGNlbnRlciwgMTAwJSwgY29sb3Itc3RvcCg0NCUsIHJnYmEoNjUsNjQsNjYsMSkpLCBjb2xvci1zdG9wKDk1JSwgcmdiYSg0Myw0Myw0MywxKSksIGNvbG9yLXN0b3AoMTAwJSwgcmdiYSg0MSw0MSw0MSwxKSkpOyAvKiBzYWZhcmk0KyxjaHJvbWUgKi9cclxuICAgIGJhY2tncm91bmQ6LXdlYmtpdC1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIHNhZmFyaTUuMSssY2hyb21lMTArICovXHJcbiAgICBiYWNrZ3JvdW5kOiAtby1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIG9wZXJhIDExLjEwKyAqL1xyXG4gICAgYmFja2dyb3VuZDogLW1zLXJhZGlhbC1ncmFkaWVudChjZW50ZXIsIGVsbGlwc2UgY292ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogaWUxMCsgKi9cclxuICAgIGJhY2tncm91bmQ6cmFkaWFsLWdyYWRpZW50KGVsbGlwc2UgYXQgY2VudGVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIHczYyAqL1xyXG4gICAgZmlsdGVyOiBwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuZ3JhZGllbnQoIHN0YXJ0Q29sb3JzdHI9JyM0MTQwNDInLCBlbmRDb2xvcnN0cj0nIzI5MjkyOScsR3JhZGllbnRUeXBlPTEgKTsgLyogaWU2LTkgKi9cclxuXHJcbn1cclxuXHJcbi8vZm9udCB3ZWlnaHRcclxuJWZ3LTEwMCwgLmZ3LTEwMHtmb250LXdlaWdodDogMTAwO31cclxuLmZ3LTQwMHtmb250LXdlaWdodDogNDAwO31cclxuXHJcbi8vZm9udC1zaXplXHJcbiVmcy0xNHB4e2ZvbnQtc2l6ZTogMTRweDt9XHJcbiVmcy0yMHB4e2ZvbnQtc2l6ZTogMjBweDt9XHJcblxyXG4vLyBjb2xvclxyXG4kY29sb3Itd2hpdGU6I2ZmZmZmZiAhaW1wb3J0YW50O1xyXG4kY29sb3Itd2hpdGUtMzpyZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcclxuJGNvbG9yLXdoaXRlLTU6cmdiYSgyNDgsIDI0OSwgMjUwLCAuNSkgIWltcG9ydGFudDtcclxuJGNvbG9yLXdoaXRlLTk6cmdiYSgyNDgsIDI0OSwgMjUwLCAuOSkgIWltcG9ydGFudDtcclxuJGNvbG9yLWJsYWNrOiMwMDAwMDAgIWltcG9ydGFudDtcclxuJHRoZW1lLWNvbG9yLWJsYWNrOiMyRjJGMkY7XHJcbiR0aGVtZS1jb2xvci1ibHVlOiMwNDYyRUU7XHJcblxyXG4udGhlbWUtY29sb3ItYmx1ZXtjb2xvcjokdGhlbWUtY29sb3ItYmx1ZTt9XHJcbi5iZy1ibGFja3sgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsYWNrO31cclxuXHJcbmlucHV0e2JvcmRlci1jb2xvcjokY29sb3Itd2hpdGU7IH1cclxuLmlucHV0LWdyb3VwLXRleHR7Y29sb3I6ICRjb2xvci13aGl0ZTtiYWNrZ3JvdW5kLWNvbG9yOiR0aGVtZS1jb2xvci1ibGFjazsgYm9yZGVyLWNvbG9yOiAkdGhlbWUtY29sb3ItYmxhY2s7fVxyXG4vL3dpZHRoc1xyXG4ubXctMTUwcHh7bWF4LXdpZHRoOiAyMDBweDsgfVxyXG4uaW5wdXQtZ3JvdXAtbGctNTB7XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDo5OTJweCkge1xyXG4gICAgICAgIG1heC13aWR0aDogMzUlO1xyXG4gICAgfVxyXG59XHJcbi8vIGJ1dHRvbnNcclxuLmJ0bjpmb2N1cywgLmJ0bi5mb2N1c3tib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7fVxyXG4uYnRuLXAxe3BhZGRpbmc6IDExcHggMjBweDt9XHJcbi5idG4tbGlnaHQge2NvbG9yOiAkY29sb3Itd2hpdGU7YmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlLTM7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtMztcclxud2lkdGg6IGF1dG87bGV0dGVyLXNwYWNpbmc6IC41cHg7Zm9udC13ZWlnaHQ6IDYwMDtoZWlnaHQ6IDQ4cHg7cGFkZGluZzogMHB4IDMwcHg7XHJcbiAgICAmOmhvdmVyLCY6Zm9jdXMsICY6YWN0aXZle2JhY2tncm91bmQtY29sb3I6ICAkY29sb3Itd2hpdGUtNTtib3JkZXItY29sb3I6ICRjb2xvci13aGl0ZS01O31cclxufVxyXG4uYnRuLXdoaXRlIHtjb2xvcjogJHRoZW1lLWNvbG9yLWJsdWU7YmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlLTk7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtMztcclxuICAgIHdpZHRoOiAxODBweDtsZXR0ZXItc3BhY2luZzogLjVweDtmb250LXdlaWdodDogNjAwO2hlaWdodDogNDhweDtcclxuICAgICAgICAmOmhvdmVyLCY6Zm9jdXMsICY6YWN0aXZle2JhY2tncm91bmQtY29sb3I6ICAkY29sb3Itd2hpdGU7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtNTt9XHJcbiAgICB9XHJcbi5idG4tcmVzcG9uc2l2ZXtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6OTkycHgpIHtcclxuICAgICAgICBwYWRkaW5nOiAwcHggNTBweDtcclxuICAgIH1cclxufVxyXG4vLyAuYnRuLWFwcGxlIHtiYWNrZ3JvdW5kLWNvbG9yOiAkYmctcmVkO2JvcmRlci1jb2xvcjogJGJnLXJlZDsgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuLy8gICAgICY6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjogJGJnLXJlZC1ob3ZlcjsgYm9yZGVyLWNvbG9yOiAkYmctcmVkLWhvdmVyO31cclxuLy8gfVxyXG5ib2R5eyBiYWNrZ3JvdW5kLWNvbG9yOiAkdGhlbWUtY29sb3ItYmx1ZTt9XHJcbkBtZWRpYSAobWF4LXdpZHRoOjk5M3B4KXtcclxuICAgIGgxe2ZvbnQtc2l6ZTogMnJlbTt9XHJcbn0iLCIucG9pbnRlciB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuKiB7XG4gIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLmZ3LTEwMCB7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG59XG5cbi5mdy00MDAge1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4udGhlbWUtY29sb3ItYmx1ZSB7XG4gIGNvbG9yOiAjMDQ2MkVFO1xufVxuXG4uYmctYmxhY2sge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG59XG5cbmlucHV0IHtcbiAgYm9yZGVyLWNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG59XG5cbi5pbnB1dC1ncm91cC10ZXh0IHtcbiAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJGMkYyRjtcbiAgYm9yZGVyLWNvbG9yOiAjMkYyRjJGO1xufVxuXG4ubXctMTUwcHgge1xuICBtYXgtd2lkdGg6IDIwMHB4O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmlucHV0LWdyb3VwLWxnLTUwIHtcbiAgICBtYXgtd2lkdGg6IDM1JTtcbiAgfVxufVxuXG4uYnRuOmZvY3VzLCAuYnRuLmZvY3VzIHtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4uYnRuLXAxIHtcbiAgcGFkZGluZzogMTFweCAyMHB4O1xufVxuXG4uYnRuLWxpZ2h0IHtcbiAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgxOTQsIDE5NSwgMTk3LCAwLjMpICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgxOTQsIDE5NSwgMTk3LCAwLjMpICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvO1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGhlaWdodDogNDhweDtcbiAgcGFkZGluZzogMHB4IDMwcHg7XG59XG4uYnRuLWxpZ2h0OmhvdmVyLCAuYnRuLWxpZ2h0OmZvY3VzLCAuYnRuLWxpZ2h0OmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjQ4LCAyNDksIDI1MCwgMC41KSAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6IHJnYmEoMjQ4LCAyNDksIDI1MCwgMC41KSAhaW1wb3J0YW50O1xufVxuXG4uYnRuLXdoaXRlIHtcbiAgY29sb3I6ICMwNDYyRUU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjQ4LCAyNDksIDI1MCwgMC45KSAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6IHJnYmEoMTk0LCAxOTUsIDE5NywgMC4zKSAhaW1wb3J0YW50O1xuICB3aWR0aDogMTgwcHg7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgaGVpZ2h0OiA0OHB4O1xufVxuLmJ0bi13aGl0ZTpob3ZlciwgLmJ0bi13aGl0ZTpmb2N1cywgLmJ0bi13aGl0ZTphY3RpdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tcmVzcG9uc2l2ZSB7XG4gIHdpZHRoOiBhdXRvO1xufVxuQG1lZGlhIChtaW4td2lkdGg6IDk5MnB4KSB7XG4gIC5idG4tcmVzcG9uc2l2ZSB7XG4gICAgcGFkZGluZzogMHB4IDUwcHg7XG4gIH1cbn1cblxuYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwNDYyRUU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTNweCkge1xuICBoMSB7XG4gICAgZm9udC1zaXplOiAycmVtO1xuICB9XG59XG4uc2xpZGUge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi9hc3NldHMvaW1hZ2VzL3Rlcm1zLWhlYWRlci5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG59IiwiQGltcG9ydCBcIi4uLy4uL192YXJpYWJsZS5zY3NzXCI7XHJcblxyXG4uc2xpZGV7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi9hc3NldHMvaW1hZ2VzL3Rlcm1zLWhlYWRlci5wbmdcIikgbm8tcmVwZWF0O2JhY2tncm91bmQtc2l6ZTogY292ZXI7YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/partner-account-guide/partner-account-guide.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/partner-account-guide/partner-account-guide.component.ts ***!
  \**************************************************************************/
/*! exports provided: PartnerAccountGuideComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnerAccountGuideComponent", function() { return PartnerAccountGuideComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PartnerAccountGuideComponent = class PartnerAccountGuideComponent {
    constructor() { }
    ngOnInit() {
    }
};
PartnerAccountGuideComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-partner-account-guide',
        template: __webpack_require__(/*! raw-loader!./partner-account-guide.component.html */ "./node_modules/raw-loader/index.js!./src/app/partner-account-guide/partner-account-guide.component.html"),
        styles: [__webpack_require__(/*! ./partner-account-guide.component.scss */ "./src/app/partner-account-guide/partner-account-guide.component.scss")]
    })
], PartnerAccountGuideComponent);



/***/ }),

/***/ "./src/app/partner-account-guide/partner-account-guide.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/partner-account-guide/partner-account-guide.module.ts ***!
  \***********************************************************************/
/*! exports provided: PartnerAccountGuideModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnerAccountGuideModule", function() { return PartnerAccountGuideModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _partner_account_guide_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partner-account-guide-routing.module */ "./src/app/partner-account-guide/partner-account-guide-routing.module.ts");
/* harmony import */ var _partner_account_guide_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./partner-account-guide.component */ "./src/app/partner-account-guide/partner-account-guide.component.ts");
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../layout/layout.module */ "./src/app/layout/layout.module.ts");






let PartnerAccountGuideModule = class PartnerAccountGuideModule {
};
PartnerAccountGuideModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_partner_account_guide_component__WEBPACK_IMPORTED_MODULE_4__["PartnerAccountGuideComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _partner_account_guide_routing_module__WEBPACK_IMPORTED_MODULE_3__["PartnerAccountGuideRoutingModule"], _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__["LayoutModule"]
        ]
    })
], PartnerAccountGuideModule);



/***/ })

}]);
//# sourceMappingURL=partner-account-guide-partner-account-guide-module-es2015.js.map