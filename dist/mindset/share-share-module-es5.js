(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["share-share-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/share/share.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/share/share.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<p>{{audoName}}</p>-->\n<!--<img src=\"{{audioImg}}\"  height=\"400\" width=\"400\"/>-->\n\n<script>\n\n \n\n\n</script>"

/***/ }),

/***/ "./src/app/share/share-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/share/share-routing.module.ts ***!
  \***********************************************/
/*! exports provided: ShareRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareRoutingModule", function() { return ShareRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _share_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./share.component */ "./src/app/share/share.component.ts");




var routes = [{ path: '', component: _share_component__WEBPACK_IMPORTED_MODULE_3__["ShareComponent"] }];
var ShareRoutingModule = /** @class */ (function () {
    function ShareRoutingModule() {
    }
    ShareRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ShareRoutingModule);
    return ShareRoutingModule;
}());



/***/ }),

/***/ "./src/app/share/share.component.scss":
/*!********************************************!*\
  !*** ./src/app/share/share.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlL3NoYXJlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/share/share.component.ts":
/*!******************************************!*\
  !*** ./src/app/share/share.component.ts ***!
  \******************************************/
/*! exports provided: ShareComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareComponent", function() { return ShareComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _commonservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../commonservice.service */ "./src/app/commonservice.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/fesm5/ngx-device-detector.js");







var ShareComponent = /** @class */ (function () {
    function ShareComponent(route, commonService, meta, router, deviceService) {
        this.route = route;
        this.commonService = commonService;
        this.meta = meta;
        this.router = router;
        this.deviceService = deviceService;
        this.queryparams = '';
        this.paramsVal = '';
        this.temp = { "result": "", "status": "" };
        this.audoName = '';
        this.audioImg = '';
    }
    ShareComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.deviceService.getDeviceInfo().os);
        //console.log(this.route.queryParams)
        /* this.route.queryParams.subscribe(__values => {
     
           this.queryparams = __values.refer;
     
         })*/
        this.route.params.subscribe(function (__values) {
            _this.paramsVal = __values.audioId;
            //console.log("Audio value:", __values.audioId)
            _this.commonService.getAudioData({ "audioId": _this.paramsVal }).subscribe(function (res) {
                _this.temp = res;
                _this.audoName = _this.temp.result[0].audioName;
                _this.audioImg = _this.temp.result[0].audioImage;
                // console.log(this.temp.result[0].audioName)
                if (typeof res == 'object') {
                    _this.meta.updateTag({ name: 'twitter:card', content: 'summary_large_image' });
                    _this.meta.updateTag({ name: 'twitter:site', content: '@mindset app' });
                    _this.meta.updateTag({ name: 'twitter:title', content: _this.temp.result[0].audioName });
                    _this.meta.updateTag({ name: 'twitter:description', content: _this.temp.result[0].audioName });
                    _this.meta.updateTag({ name: 'twitter:image', content: _this.temp.result[0].audioImage });
                    _this.meta.updateTag({ property: 'og:title', content: _this.temp.result[0].audioImage });
                    _this.meta.updateTag({ property: 'og:url', content: document.location.href });
                    _this.meta.updateTag({ property: 'og:image', content: _this.temp.result[0].audioImage });
                    _this.meta.updateTag({ property: 'og:description', content: _this.temp.result[0].audioName });
                }
            });
            if (_this.deviceService.getDeviceInfo().os == 'iOS') {
            }
            else {
                console.log(document.location.href);
                var myurl = 'android-app://com.mindset.motivation/' + document.location.href;
                _this.router.navigateByUrl(myurl).then(function (e) {
                    if (e) {
                        console.log("Navigation is successful!");
                    }
                    else {
                        console.log("Navigation has failed!");
                    }
                });
                //window.location.href = 'android-app://com.apps.mindset/'+document.location.href
            }
        });
    };
    ShareComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _commonservice_service__WEBPACK_IMPORTED_MODULE_3__["CommonserviceService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Meta"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: ngx_device_detector__WEBPACK_IMPORTED_MODULE_5__["DeviceDetectorService"] }
    ]; };
    ShareComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-share',
            template: __webpack_require__(/*! raw-loader!./share.component.html */ "./node_modules/raw-loader/index.js!./src/app/share/share.component.html"),
            styles: [__webpack_require__(/*! ./share.component.scss */ "./src/app/share/share.component.scss")]
        })
    ], ShareComponent);
    return ShareComponent;
}());



/***/ }),

/***/ "./src/app/share/share.module.ts":
/*!***************************************!*\
  !*** ./src/app/share/share.module.ts ***!
  \***************************************/
/*! exports provided: ShareModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareModule", function() { return ShareModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _share_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./share-routing.module */ "./src/app/share/share-routing.module.ts");
/* harmony import */ var _share_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./share.component */ "./src/app/share/share.component.ts");
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../layout/layout.module */ "./src/app/layout/layout.module.ts");






var ShareModule = /** @class */ (function () {
    function ShareModule() {
    }
    ShareModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_share_component__WEBPACK_IMPORTED_MODULE_4__["ShareComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _share_routing_module__WEBPACK_IMPORTED_MODULE_3__["ShareRoutingModule"], _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__["LayoutModule"]
            ]
        })
    ], ShareModule);
    return ShareModule;
}());



/***/ })

}]);
//# sourceMappingURL=share-share-module-es5.js.map