(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    { path: '', loadChildren: function () { return Promise.all(/*! import() | home-home-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("home-home-module")]).then(__webpack_require__.bind(null, /*! ./home/home.module */ "./src/app/home/home.module.ts")).then(function (m) { return m.HomeModule; }); } },
    { path: 'contact-us', loadChildren: function () { return Promise.all(/*! import() | contact-us-contact-us-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("common"), __webpack_require__.e("contact-us-contact-us-module")]).then(__webpack_require__.bind(null, /*! ./contact-us/contact-us.module */ "./src/app/contact-us/contact-us.module.ts")).then(function (m) { return m.ContactUsModule; }); } },
    { path: 'privacy-policy', loadChildren: function () { return Promise.all(/*! import() | privacy-policy-privacy-policy-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("privacy-policy-privacy-policy-module")]).then(__webpack_require__.bind(null, /*! ./privacy-policy/privacy-policy.module */ "./src/app/privacy-policy/privacy-policy.module.ts")).then(function (m) { return m.PrivacyPolicyModule; }); } },
    { path: 'terms', loadChildren: function () { return Promise.all(/*! import() | terms-terms-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("terms-terms-module")]).then(__webpack_require__.bind(null, /*! ./terms/terms.module */ "./src/app/terms/terms.module.ts")).then(function (m) { return m.TermsModule; }); } },
    { path: 'creators', loadChildren: function () { return Promise.all(/*! import() | creators-creators-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("creators-creators-module")]).then(__webpack_require__.bind(null, /*! ./creators/creators.module */ "./src/app/creators/creators.module.ts")).then(function (m) { return m.CreatorsModule; }); } },
    { path: 'speakers', loadChildren: function () { return Promise.all(/*! import() | creators-creators-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("creators-creators-module")]).then(__webpack_require__.bind(null, /*! ./creators/creators.module */ "./src/app/creators/creators.module.ts")).then(function (m) { return m.CreatorsModule; }); } },
    { path: 'faqs-for-partners', loadChildren: function () { return Promise.all(/*! import() | faqs-for-partners-faqs-for-partners-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("faqs-for-partners-faqs-for-partners-module")]).then(__webpack_require__.bind(null, /*! ./faqs-for-partners/faqs-for-partners.module */ "./src/app/faqs-for-partners/faqs-for-partners.module.ts")).then(function (m) { return m.FaqsForPartnersModule; }); } },
    { path: 'faqs-for-users', loadChildren: function () { return Promise.all(/*! import() | faqs-for-users-faqs-for-users-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("faqs-for-users-faqs-for-users-module")]).then(__webpack_require__.bind(null, /*! ./faqs-for-users/faqs-for-users.module */ "./src/app/faqs-for-users/faqs-for-users.module.ts")).then(function (m) { return m.FaqsForUsersModule; }); } },
    { path: 'partner-account-guide', loadChildren: function () { return Promise.all(/*! import() | partner-account-guide-partner-account-guide-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("partner-account-guide-partner-account-guide-module")]).then(__webpack_require__.bind(null, /*! ./partner-account-guide/partner-account-guide.module */ "./src/app/partner-account-guide/partner-account-guide.module.ts")).then(function (m) { return m.PartnerAccountGuideModule; }); } },
    { path: 'verified-partner-account', loadChildren: function () { return Promise.all(/*! import() | verified-partner-account-verified-partner-account-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("verified-partner-account-verified-partner-account-module")]).then(__webpack_require__.bind(null, /*! ./verified-partner-account/verified-partner-account.module */ "./src/app/verified-partner-account/verified-partner-account.module.ts")).then(function (m) { return m.VerifiedPartnerAccountModule; }); } },
    { path: 'share/:audioId', loadChildren: function () { return Promise.all(/*! import() | share-share-module */[__webpack_require__.e("default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"), __webpack_require__.e("common"), __webpack_require__.e("share-share-module")]).then(__webpack_require__.bind(null, /*! ./share/share.module */ "./src/app/share/share.module.ts")).then(function (m) { return m.ShareModule; }); } }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(titleService, metaService) {
        this.titleService = titleService;
        this.metaService = metaService;
        this.title = 'mindset';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.titleService.setTitle(this.title);
        this.metaService.addTags([
            { name: 'keywords', content: 'Angular, Universal, Example' },
            { name: 'description', content: 'Angular Universal Example' },
            { name: 'robots', content: 'index, follow' }
        ]);
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/fesm5/ngx-device-detector.js");









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                ngx_device_detector__WEBPACK_IMPORTED_MODULE_8__["DeviceDetectorModule"].forRoot()
            ],
            providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_7__["APP_BASE_HREF"], useValue: '/' }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\angular\mindset_web\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map