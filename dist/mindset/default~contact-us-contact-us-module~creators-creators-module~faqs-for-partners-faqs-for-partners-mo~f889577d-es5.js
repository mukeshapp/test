(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/footer/footer.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/footer/footer.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"text-white d-lg-flex justify-content-end px-4\">\r\n    <ul class=\" list-inline list-underline\">\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/speakers\" [ngClass]=\"{'d-none': speakersPage}\" >For Speakers & Creators</li>\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/contact-us\" >Contact Us</li>\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/faqs-for-users\" [ngClass]=\"{'d-none': speakersPage}\">FAQ's for Users</li>\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/faqs-for-partners\" [ngClass]=\"{'d-none': !speakersPage}\">FAQ's for Partners</li>\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/privacy-policy\" >Privacy</li>\r\n        <li class=\"list-inline-item mr-4 mr-xl-5 pointer\" routerLink=\"/terms\" >Terms and Conditions</li>\r\n    </ul>\r\n    <div class=\"d-flex justify-content-between\">\r\n        <p class=\"mr-5\">&copy; Mindset 2019</p>\r\n        <ul class=\"list-inline\">\r\n            <a class=\"list-inline-item pointer mr-4 text-white\" href=\"https://www.instagram.com/unlock.mindset/\" target=\"_blank\"><i class=\"fab fa-2x fa-instagram\"></i></a>\r\n            <a class=\"list-inline-item pointer text-white\" href=\"https://twitter.com/unlockmindset\" target=\"_blank\"><i class=\"fab fa-2x fa-twitter\"></i></a>\r\n        </ul>\r\n    </div>\r\n</footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/header/header.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/header/header.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"w-100 fixed-top d-flex p-4\">\r\n    <div class=\"logo pointer\" routerLink=\"/\">\r\n        <img src=\"../../../assets/images/mindset_logo_white.png\" />\r\n    </div>\r\n    <!-- <div>\r\n        <button class=\"btn btn-light mr-4\">For Speakers</button>\r\n        <button class=\"btn btn-light\">For Creators</button>\r\n    </div> -->\r\n</div>"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pointer {\n  cursor: pointer;\n}\n\n* {\n  outline: none !important;\n}\n\n.fw-100 {\n  font-weight: 100;\n}\n\n.fw-400 {\n  font-weight: 400;\n}\n\nfooter {\n  font-size: 14px;\n}\n\n.theme-color-blue {\n  color: #0462EE;\n}\n\n.bg-black {\n  background-color: #000000 !important;\n}\n\ninput {\n  border-color: #ffffff !important;\n}\n\n.input-group-text {\n  color: #ffffff !important;\n  background-color: #2F2F2F;\n  border-color: #2F2F2F;\n}\n\n.mw-150px {\n  max-width: 200px;\n}\n\n@media (min-width: 992px) {\n  .input-group-lg-50 {\n    max-width: 35%;\n  }\n}\n\n.btn:focus, .btn.focus {\n  box-shadow: none !important;\n}\n\n.btn-p1 {\n  padding: 11px 20px;\n}\n\n.btn-light {\n  color: #ffffff !important;\n  background-color: rgba(194, 195, 197, 0.3) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: auto;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n  padding: 0px 30px;\n}\n\n.btn-light:hover, .btn-light:focus, .btn-light:active {\n  background-color: rgba(248, 249, 250, 0.5) !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-white {\n  color: #0462EE;\n  background-color: rgba(248, 249, 250, 0.9) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: 180px;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n}\n\n.btn-white:hover, .btn-white:focus, .btn-white:active {\n  background-color: #ffffff !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-responsive {\n  width: auto;\n}\n\n@media (min-width: 992px) {\n  .btn-responsive {\n    padding: 0px 50px;\n  }\n}\n\nbody {\n  background-color: #0462EE;\n}\n\n@media (max-width: 993px) {\n  h1 {\n    font-size: 2rem;\n  }\n}\n\n.fa-2x {\n  font-size: 1.5rem;\n}\n\n@media (max-width: 992px) {\n  .fixed-bottom {\n    position: relative;\n  }\n\n  .list-underline {\n    text-align: center;\n  }\n  .list-underline li {\n    text-decoration: underline;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2Zvb3Rlci9EOlxcYW5ndWxhclxcbWluZHNldF93ZWIvc3JjXFxhcHBcXGxheW91dFxcZm9vdGVyXFxmb290ZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvZm9vdGVyL0Q6XFxhbmd1bGFyXFxtaW5kc2V0X3dlYi9zdGRpbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFTLGVBQUE7QUNFVDs7QUREQTtFQUFFLHdCQUFBO0FDS0Y7O0FEa0JBO0VBQWlCLGdCQUFBO0FDZGpCOztBRGVBO0VBQVEsZ0JBQUE7QUNYUjs7QURjQTtFQUFTLGVBQUE7QUNWVDs7QURzQkE7RUFBa0IsY0FGQTtBQ2hCbEI7O0FEbUJBO0VBQVcsb0NBTEU7QUNWYjs7QURpQkE7RUFBTSxnQ0FYTztBQ0ZiOztBRGNBO0VBQWtCLHlCQVpMO0VBWXlCLHlCQVBuQjtFQU93RCxxQkFQeEQ7QUNEbkI7O0FEVUE7RUFBVSxnQkFBQTtBQ05WOztBRFFJO0VBREo7SUFFUSxjQUFBO0VDSk47QUFDRjs7QURPQTtFQUF1QiwyQkFBQTtBQ0h2Qjs7QURJQTtFQUFRLGtCQUFBO0FDQVI7O0FEQ0E7RUFBWSx5QkF2QkM7RUF1Qm1CLHFEQXRCakI7RUFzQmtELGlEQXRCbEQ7RUF1QmYsV0FBQTtFQUFZLHFCQUFBO0VBQXFCLGdCQUFBO0VBQWlCLFlBQUE7RUFBYSxpQkFBQTtBQ1MvRDs7QURSSTtFQUEwQixxREF2QmY7RUF1QmlELGlEQXZCakQ7QUNtQ2Y7O0FEVkE7RUFBWSxjQXJCTTtFQXFCbUIscURBeEJ0QjtFQXdCdUQsaURBMUJ2RDtFQTJCWCxZQUFBO0VBQWEscUJBQUE7RUFBcUIsZ0JBQUE7RUFBaUIsWUFBQTtBQ21CdkQ7O0FEbEJRO0VBQTBCLG9DQTdCckI7RUE2QnFELGlEQTNCbkQ7QUNpRGY7O0FEcEJBO0VBQ0ksV0FBQTtBQ3VCSjs7QUR0Qkk7RUFGSjtJQUdRLGlCQUFBO0VDeUJOO0FBQ0Y7O0FEcEJBO0VBQU0seUJBbENZO0FDMERsQjs7QUR2QkE7RUFDSTtJQUFHLGVBQUE7RUMyQkw7QUFDRjs7QUNwR0E7RUFBTyxpQkFBQTtBRHVHUDs7QUN0R0E7RUFDSTtJQUFjLGtCQUFBO0VEMEdoQjs7RUN6R0U7SUFBaUIsa0JBQUE7RUQ2R25CO0VDNUdNO0lBQUcsMEJBQUE7RUQrR1Q7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBvaW50ZXJ7Y3Vyc29yOiBwb2ludGVyO31cclxuKntvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7fVxyXG5cclxuXHJcbi8vICB0cmFuc2l0aW9uXHJcbiV0cmFuc2l0aW9uey13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLW91dDstbW96LXRyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1vdXQ7LW8tdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLW91dDt0cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2Utb3V0O31cclxuJXRyYW5zaXRpb24xey13ZWJraXQtdHJhbnNpdGlvbjogLjVzIGVhc2UtaW4tb3V0Oy1tb3otdHJhbnNpdGlvbjogLjVzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IC41cyBlYXNlLWluLW91dDt0cmFuc2l0aW9uOiAuNXMgZWFzZS1pbi1vdXQ7fVxyXG4ldHJhbnNpdGlvbjJ7LXdlYmtpdC10cmFuc2l0aW9uOiB3aWR0aCAycyBlYXNlLWluLW91dDstbW96LXRyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0O3RyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0O31cclxuJXRyYW5zaXRpb24zey13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDJzIGVhc2UtaW4tb3V0Oy1tb3otdHJhbnNpdGlvbjogYWxsIDJzIGVhc2UtaW4tb3V0Oy1vLXRyYW5zaXRpb246IGFsbCAycyBlYXNlLWluLW91dDt0cmFuc2l0aW9uOiBhbGwgMnMgZWFzZS1pbi1vdXQ7fVxyXG5cclxuLy9ncmFkaWFudFxyXG4lYmxhY2stYmd7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgYmFja2dyb3VuZDogLW1vei1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIGZmMy42KyAqL1xyXG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1ncmFkaWVudChyYWRpYWwsIGNlbnRlciBjZW50ZXIsIDBweCwgY2VudGVyIGNlbnRlciwgMTAwJSwgY29sb3Itc3RvcCg0NCUsIHJnYmEoNjUsNjQsNjYsMSkpLCBjb2xvci1zdG9wKDk1JSwgcmdiYSg0Myw0Myw0MywxKSksIGNvbG9yLXN0b3AoMTAwJSwgcmdiYSg0MSw0MSw0MSwxKSkpOyAvKiBzYWZhcmk0KyxjaHJvbWUgKi9cclxuICAgIGJhY2tncm91bmQ6LXdlYmtpdC1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIHNhZmFyaTUuMSssY2hyb21lMTArICovXHJcbiAgICBiYWNrZ3JvdW5kOiAtby1yYWRpYWwtZ3JhZGllbnQoY2VudGVyLCBlbGxpcHNlIGNvdmVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIG9wZXJhIDExLjEwKyAqL1xyXG4gICAgYmFja2dyb3VuZDogLW1zLXJhZGlhbC1ncmFkaWVudChjZW50ZXIsIGVsbGlwc2UgY292ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogaWUxMCsgKi9cclxuICAgIGJhY2tncm91bmQ6cmFkaWFsLWdyYWRpZW50KGVsbGlwc2UgYXQgY2VudGVyLCByZ2JhKDY1LDY0LDY2LDEpIDQ0JSwgcmdiYSg0Myw0Myw0MywxKSA5NSUsIHJnYmEoNDEsNDEsNDEsMSkgMTAwJSk7IC8qIHczYyAqL1xyXG4gICAgZmlsdGVyOiBwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuZ3JhZGllbnQoIHN0YXJ0Q29sb3JzdHI9JyM0MTQwNDInLCBlbmRDb2xvcnN0cj0nIzI5MjkyOScsR3JhZGllbnRUeXBlPTEgKTsgLyogaWU2LTkgKi9cclxuXHJcbn1cclxuXHJcbi8vZm9udCB3ZWlnaHRcclxuJWZ3LTEwMCwgLmZ3LTEwMHtmb250LXdlaWdodDogMTAwO31cclxuLmZ3LTQwMHtmb250LXdlaWdodDogNDAwO31cclxuXHJcbi8vZm9udC1zaXplXHJcbiVmcy0xNHB4e2ZvbnQtc2l6ZTogMTRweDt9XHJcbiVmcy0yMHB4e2ZvbnQtc2l6ZTogMjBweDt9XHJcblxyXG4vLyBjb2xvclxyXG4kY29sb3Itd2hpdGU6I2ZmZmZmZiAhaW1wb3J0YW50O1xyXG4kY29sb3Itd2hpdGUtMzpyZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcclxuJGNvbG9yLXdoaXRlLTU6cmdiYSgyNDgsIDI0OSwgMjUwLCAuNSkgIWltcG9ydGFudDtcclxuJGNvbG9yLXdoaXRlLTk6cmdiYSgyNDgsIDI0OSwgMjUwLCAuOSkgIWltcG9ydGFudDtcclxuJGNvbG9yLWJsYWNrOiMwMDAwMDAgIWltcG9ydGFudDtcclxuJHRoZW1lLWNvbG9yLWJsYWNrOiMyRjJGMkY7XHJcbiR0aGVtZS1jb2xvci1ibHVlOiMwNDYyRUU7XHJcblxyXG4udGhlbWUtY29sb3ItYmx1ZXtjb2xvcjokdGhlbWUtY29sb3ItYmx1ZTt9XHJcbi5iZy1ibGFja3sgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsYWNrO31cclxuXHJcbmlucHV0e2JvcmRlci1jb2xvcjokY29sb3Itd2hpdGU7IH1cclxuLmlucHV0LWdyb3VwLXRleHR7Y29sb3I6ICRjb2xvci13aGl0ZTtiYWNrZ3JvdW5kLWNvbG9yOiR0aGVtZS1jb2xvci1ibGFjazsgYm9yZGVyLWNvbG9yOiAkdGhlbWUtY29sb3ItYmxhY2s7fVxyXG4vL3dpZHRoc1xyXG4ubXctMTUwcHh7bWF4LXdpZHRoOiAyMDBweDsgfVxyXG4uaW5wdXQtZ3JvdXAtbGctNTB7XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDo5OTJweCkge1xyXG4gICAgICAgIG1heC13aWR0aDogMzUlO1xyXG4gICAgfVxyXG59XHJcbi8vIGJ1dHRvbnNcclxuLmJ0bjpmb2N1cywgLmJ0bi5mb2N1c3tib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7fVxyXG4uYnRuLXAxe3BhZGRpbmc6IDExcHggMjBweDt9XHJcbi5idG4tbGlnaHQge2NvbG9yOiAkY29sb3Itd2hpdGU7YmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlLTM7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtMztcclxud2lkdGg6IGF1dG87bGV0dGVyLXNwYWNpbmc6IC41cHg7Zm9udC13ZWlnaHQ6IDYwMDtoZWlnaHQ6IDQ4cHg7cGFkZGluZzogMHB4IDMwcHg7XHJcbiAgICAmOmhvdmVyLCY6Zm9jdXMsICY6YWN0aXZle2JhY2tncm91bmQtY29sb3I6ICAkY29sb3Itd2hpdGUtNTtib3JkZXItY29sb3I6ICRjb2xvci13aGl0ZS01O31cclxufVxyXG4uYnRuLXdoaXRlIHtjb2xvcjogJHRoZW1lLWNvbG9yLWJsdWU7YmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlLTk7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtMztcclxuICAgIHdpZHRoOiAxODBweDtsZXR0ZXItc3BhY2luZzogLjVweDtmb250LXdlaWdodDogNjAwO2hlaWdodDogNDhweDtcclxuICAgICAgICAmOmhvdmVyLCY6Zm9jdXMsICY6YWN0aXZle2JhY2tncm91bmQtY29sb3I6ICAkY29sb3Itd2hpdGU7Ym9yZGVyLWNvbG9yOiAkY29sb3Itd2hpdGUtNTt9XHJcbiAgICB9XHJcbi5idG4tcmVzcG9uc2l2ZXtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6OTkycHgpIHtcclxuICAgICAgICBwYWRkaW5nOiAwcHggNTBweDtcclxuICAgIH1cclxufVxyXG4vLyAuYnRuLWFwcGxlIHtiYWNrZ3JvdW5kLWNvbG9yOiAkYmctcmVkO2JvcmRlci1jb2xvcjogJGJnLXJlZDsgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuLy8gICAgICY6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjogJGJnLXJlZC1ob3ZlcjsgYm9yZGVyLWNvbG9yOiAkYmctcmVkLWhvdmVyO31cclxuLy8gfVxyXG5ib2R5eyBiYWNrZ3JvdW5kLWNvbG9yOiAkdGhlbWUtY29sb3ItYmx1ZTt9XHJcbkBtZWRpYSAobWF4LXdpZHRoOjk5M3B4KXtcclxuICAgIGgxe2ZvbnQtc2l6ZTogMnJlbTt9XHJcbn0iLCIucG9pbnRlciB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuKiB7XG4gIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLmZ3LTEwMCB7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG59XG5cbi5mdy00MDAge1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG5mb290ZXIge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi50aGVtZS1jb2xvci1ibHVlIHtcbiAgY29sb3I6ICMwNDYyRUU7XG59XG5cbi5iZy1ibGFjayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQge1xuICBib3JkZXItY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbn1cblxuLmlucHV0LWdyb3VwLXRleHQge1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMkYyRjJGO1xuICBib3JkZXItY29sb3I6ICMyRjJGMkY7XG59XG5cbi5tdy0xNTBweCB7XG4gIG1heC13aWR0aDogMjAwcHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge1xuICAuaW5wdXQtZ3JvdXAtbGctNTAge1xuICAgIG1heC13aWR0aDogMzUlO1xuICB9XG59XG5cbi5idG46Zm9jdXMsIC5idG4uZm9jdXMge1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tcDEge1xuICBwYWRkaW5nOiAxMXB4IDIwcHg7XG59XG5cbi5idG4tbGlnaHQge1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcbiAgd2lkdGg6IGF1dG87XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgaGVpZ2h0OiA0OHB4O1xuICBwYWRkaW5nOiAwcHggMzBweDtcbn1cbi5idG4tbGlnaHQ6aG92ZXIsIC5idG4tbGlnaHQ6Zm9jdXMsIC5idG4tbGlnaHQ6YWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjUpICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5idG4td2hpdGUge1xuICBjb2xvcjogIzA0NjJFRTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjkpICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgxOTQsIDE5NSwgMTk3LCAwLjMpICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxODBweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBoZWlnaHQ6IDQ4cHg7XG59XG4uYnRuLXdoaXRlOmhvdmVyLCAuYnRuLXdoaXRlOmZvY3VzLCAuYnRuLXdoaXRlOmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDI0OCwgMjQ5LCAyNTAsIDAuNSkgIWltcG9ydGFudDtcbn1cblxuLmJ0bi1yZXNwb25zaXZlIHtcbiAgd2lkdGg6IGF1dG87XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmJ0bi1yZXNwb25zaXZlIHtcbiAgICBwYWRkaW5nOiAwcHggNTBweDtcbiAgfVxufVxuXG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA0NjJFRTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDk5M3B4KSB7XG4gIGgxIHtcbiAgICBmb250LXNpemU6IDJyZW07XG4gIH1cbn1cbi5mYS0yeCB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmZpeGVkLWJvdHRvbSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgLmxpc3QtdW5kZXJsaW5lIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmxpc3QtdW5kZXJsaW5lIGxpIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgfVxufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi92YXJpYWJsZVwiO1xyXG5cclxuLmZhLTJ4e2ZvbnQtc2l6ZTogMS41cmVtO31cclxuQG1lZGlhIChtYXgtd2lkdGg6OTkycHgpe1xyXG4gICAgLmZpeGVkLWJvdHRvbXtwb3NpdGlvbjogcmVsYXRpdmU7fVxyXG4gICAgLmxpc3QtdW5kZXJsaW5lIHt0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbGl7dGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7fVxyXG4gICAgfVxyXG5cclxufVxyXG5mb290ZXJ7XHJcbiAgICBAZXh0ZW5kICVmcy0xNHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(router) {
        this.router = router;
    }
    FooterComponent.prototype.ngOnInit = function () {
        if (this.router.url == "/") {
            $('footer').addClass('fixed-bottom');
        }
        else if (this.router.url == "/speakers") {
            this.speakersPage = true;
        }
        else {
            this.speakersPage = false;
        }
    };
    FooterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layout/footer/footer.component.scss")]
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/header.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".logo img {\n  width: 170px;\n}\n\n@media (max-width: 992px) {\n  .fixed-top {\n    position: absolute;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2hlYWRlci9EOlxcYW5ndWxhclxcbWluZHNldF93ZWIvc3JjXFxhcHBcXGxheW91dFxcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQUksWUFBQTtBQ0NSOztBRENBO0VBQ0k7SUFBVyxrQkFBQTtFQ0diO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dve1xyXG4gICAgaW1ne3dpZHRoOiAxNzBweDt9XHJcbn1cclxuQG1lZGlhIChtYXgtd2lkdGg6OTkycHgpe1xyXG4gICAgLmZpeGVkLXRvcHtwb3NpdGlvbjogYWJzb2x1dGU7fVxyXG59IiwiLmxvZ28gaW1nIHtcbiAgd2lkdGg6IDE3MHB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmZpeGVkLXRvcCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/header/header.component.scss")]
        })
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/layout/layout-routing.module.ts ***!
  \*************************************************/
/*! exports provided: LayoutRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutRoutingModule", function() { return LayoutRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/layout.module.ts":
/*!*****************************************!*\
  !*** ./src/app/layout/layout.module.ts ***!
  \*****************************************/
/*! exports provided: LayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _layout_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout-routing.module */ "./src/app/layout/layout-routing.module.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/header.component */ "./src/app/layout/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/layout/footer/footer.component.ts");






var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"]],
            exports: [_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _layout_routing_module__WEBPACK_IMPORTED_MODULE_3__["LayoutRoutingModule"],
            ]
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~contact-us-contact-us-module~creators-creators-module~faqs-for-partners-faqs-for-partners-mo~f889577d-es5.js.map