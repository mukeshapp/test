(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/commonservice.service.ts":
/*!******************************************!*\
  !*** ./src/app/commonservice.service.ts ***!
  \******************************************/
/*! exports provided: CommonserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonserviceService", function() { return CommonserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let CommonserviceService = class CommonserviceService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.apiBaseURL = "http://ec2-18-189-171-250.us-east-2.compute.amazonaws.com:3000/";
        this.apiURL = this.apiBaseURL + '/api/web-contact-us';
        this.apiURL2 = this.apiBaseURL + '/api/addSubscription';
        this.apiGetAudio = this.apiBaseURL + '/api/get-audios-info';
    }
    sendMessage(formdata) {
        console.log('formdata', formdata);
        return this.httpClient.post(`${this.apiURL}`, formdata);
    }
    getAudioData(formdata) {
        //console.log('formdata', formdata);
        return this.httpClient.post(`${this.apiGetAudio}`, formdata);
    }
    sendSubscribe(fdata) {
        //https://us20.api.mailchimp.com/3.0/lists/888a9acccd/members
        /*
         
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json;charset=utf-8',
              'Authorization': 'Basic ' + btoa('username:79b40321b828a7f7ad0d064631c3bf41'),
              //'Authorization': 'Basic 79b40321b828a7f7ad0d064631c3bf41',
             
            })
          }
      
          
          // console.log(fdata)
          let formdata ={
            'email_address':fdata.email,
            'status': 'subscribed',
            'merge_fields': {
              'FNAME':'',
              'LNAME':''
            }
          }
          
         return this.httpClient.post(`https://us20.api.mailchimp.com/3.0/lists/888a9acccd/members`,formdata,httpOptions)
        
        */
        return this.httpClient.post(`${this.apiURL2}`, fdata);
    }
};
CommonserviceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CommonserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CommonserviceService);



/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map