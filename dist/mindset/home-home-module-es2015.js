(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<div class=\"pattern\">\r\n    <div class=\"black-bg\">\r\n        <img src=\"../../assets/images/home-mob.png\"/>\r\n    </div>\r\n    <div class=\"blue-bg\">\r\n        <h1>The Ultimate Source for Motivation</h1>\r\n        <p>Listen to your favorite motivational speeches and get ready for the day.<br>\r\n            <strong class=\"my-2 d-block\">Download Mindset App today.</strong></p>\r\n        <div class=\"d-lg-flex\">\r\n           <a href=\"https://apps.apple.com/in/app/mindset-motivation-self-care/id1487761500\"> <img class=\"mr-lg-3 mb-3 mb-lg-0 mw-150px\" src=\"../../assets/images/svg/app-store-apple.svg\"/></a>\r\n           <a href=\"https://play.google.com/store/apps/details?id=com.mindset.app&pageId=none\"> <img class=\"mw-150px\" src=\"../../assets/images/svg/google-play-badge.svg\"/></a>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"d-flex p-4 absolute-tr-btns\">\r\n    <button class=\"btn btn-light pointer\" routerLink=\"/speakers\">For Speakers & Creators</button>\r\n</div>\r\n<app-footer class=\"mb-2\"></app-footer>"

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");




const routes = [{ path: '', component: _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] }];
let HomeRoutingModule = class HomeRoutingModule {
};
HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomeRoutingModule);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pointer {\n  cursor: pointer;\n}\n\n* {\n  outline: none !important;\n}\n\n.fw-100 {\n  font-weight: 100;\n}\n\n.fw-400 {\n  font-weight: 400;\n}\n\n.theme-color-blue {\n  color: #0462EE;\n}\n\n.bg-black {\n  background-color: #000000 !important;\n}\n\ninput {\n  border-color: #ffffff !important;\n}\n\n.input-group-text {\n  color: #ffffff !important;\n  background-color: #2F2F2F;\n  border-color: #2F2F2F;\n}\n\n.mw-150px {\n  max-width: 200px;\n}\n\n@media (min-width: 992px) {\n  .input-group-lg-50 {\n    max-width: 35%;\n  }\n}\n\n.btn:focus, .btn.focus {\n  box-shadow: none !important;\n}\n\n.btn-p1 {\n  padding: 11px 20px;\n}\n\n.btn-light {\n  color: #ffffff !important;\n  background-color: rgba(194, 195, 197, 0.3) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: auto;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n  padding: 0px 30px;\n}\n\n.btn-light:hover, .btn-light:focus, .btn-light:active {\n  background-color: rgba(248, 249, 250, 0.5) !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-white {\n  color: #0462EE;\n  background-color: rgba(248, 249, 250, 0.9) !important;\n  border-color: rgba(194, 195, 197, 0.3) !important;\n  width: 180px;\n  letter-spacing: 0.5px;\n  font-weight: 600;\n  height: 48px;\n}\n\n.btn-white:hover, .btn-white:focus, .btn-white:active {\n  background-color: #ffffff !important;\n  border-color: rgba(248, 249, 250, 0.5) !important;\n}\n\n.btn-responsive {\n  width: auto;\n}\n\n@media (min-width: 992px) {\n  .btn-responsive {\n    padding: 0px 50px;\n  }\n}\n\nbody {\n  background-color: #0462EE;\n}\n\n@media (max-width: 993px) {\n  h1 {\n    font-size: 2rem;\n  }\n}\n\n.black-bg {\n  display: -webkit-inline-box;\n  display: inline-flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-transform-origin: top left;\n          transform-origin: top left;\n}\n\n@media (min-width: 993px) {\n  .black-bg {\n    width: 61%;\n    height: 100vh;\n    -webkit-transform: skewX(-37deg);\n            transform: skewX(-37deg);\n  }\n  .black-bg img {\n    -webkit-transform: skewX(37deg);\n            transform: skewX(37deg);\n    height: 70vh;\n    margin-left: 20vh;\n  }\n}\n\n@media (min-width: 993px) and (min-height: 800px) {\n  .black-bg {\n    -webkit-transform: skewX(-27deg);\n            transform: skewX(-27deg);\n  }\n  .black-bg img {\n    -webkit-transform: skewX(27deg);\n            transform: skewX(27deg);\n  }\n}\n\n@media (max-width: 992px) {\n  .black-bg {\n    width: 100%;\n    height: 75vh;\n    -webkit-transform: skewy(-13deg);\n            transform: skewy(-13deg);\n  }\n  .black-bg img {\n    -webkit-transform: skewY(13deg);\n            transform: skewY(13deg);\n    height: 63vh;\n    margin-top: 28vh;\n  }\n}\n\n@media (min-width: 993px) and (max-width: 1200px) {\n  .black-bg img {\n    margin-left: 56vh;\n  }\n}\n\n.blue-bg {\n  color: #ffffff !important;\n  z-index: 1;\n}\n\n@media (min-width: 993px) {\n  .blue-bg {\n    position: absolute;\n    top: 32vh;\n    right: 7%;\n    width: 40%;\n    z-index: 101;\n  }\n}\n\n@media (max-width: 993px) {\n  .blue-bg {\n    position: relative;\n    width: 100%;\n    padding: 15px;\n  }\n  .blue-bg h1 {\n    font-size: 2rem;\n  }\n}\n\n@media (min-width: 992px) {\n  .absolute-tr-btns {\n    position: absolute;\n    right: 0px;\n    top: 0;\n    z-index: 2000;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9EOlxcYW5ndWxhclxcbWluZHNldF93ZWIvc3JjXFxhcHBcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9EOlxcYW5ndWxhclxcbWluZHNldF93ZWIvc3RkaW4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBUyxlQUFBO0FDRVQ7O0FEREE7RUFBRSx3QkFBQTtBQ0tGOztBRGtCQTtFQUFpQixnQkFBQTtBQ2RqQjs7QURlQTtFQUFRLGdCQUFBO0FDWFI7O0FEMEJBO0VBQWtCLGNBRkE7QUNwQmxCOztBRHVCQTtFQUFXLG9DQUxFO0FDZGI7O0FEcUJBO0VBQU0sZ0NBWE87QUNOYjs7QURrQkE7RUFBa0IseUJBWkw7RUFZeUIseUJBUG5CO0VBT3dELHFCQVB4RDtBQ0xuQjs7QURjQTtFQUFVLGdCQUFBO0FDVlY7O0FEWUk7RUFESjtJQUVRLGNBQUE7RUNSTjtBQUNGOztBRFdBO0VBQXVCLDJCQUFBO0FDUHZCOztBRFFBO0VBQVEsa0JBQUE7QUNKUjs7QURLQTtFQUFZLHlCQXZCQztFQXVCbUIscURBdEJqQjtFQXNCa0QsaURBdEJsRDtFQXVCZixXQUFBO0VBQVkscUJBQUE7RUFBcUIsZ0JBQUE7RUFBaUIsWUFBQTtFQUFhLGlCQUFBO0FDSy9EOztBREpJO0VBQTBCLHFEQXZCZjtFQXVCaUQsaURBdkJqRDtBQytCZjs7QUROQTtFQUFZLGNBckJNO0VBcUJtQixxREF4QnRCO0VBd0J1RCxpREExQnZEO0VBMkJYLFlBQUE7RUFBYSxxQkFBQTtFQUFxQixnQkFBQTtFQUFpQixZQUFBO0FDZXZEOztBRGRRO0VBQTBCLG9DQTdCckI7RUE2QnFELGlEQTNCbkQ7QUM2Q2Y7O0FEaEJBO0VBQ0ksV0FBQTtBQ21CSjs7QURsQkk7RUFGSjtJQUdRLGlCQUFBO0VDcUJOO0FBQ0Y7O0FEaEJBO0VBQU0seUJBbENZO0FDc0RsQjs7QURuQkE7RUFDSTtJQUFHLGVBQUE7RUN1Qkw7QUFDRjs7QUNoR0E7RUFDSSwyQkFBQTtFQUFBLG9CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0FEa0dKOztBQ2pHSTtFQUxKO0lBSzhCLFVBQUE7SUFBVyxhQUFBO0lBQWMsZ0NBQUE7WUFBQSx3QkFBQTtFRHVHckQ7RUN0R007SUFBSSwrQkFBQTtZQUFBLHVCQUFBO0lBQXdCLFlBQUE7SUFBYSxpQkFBQTtFRDJHL0M7QUFDRjs7QUMxR0k7RUFSSjtJQVNRLGdDQUFBO1lBQUEsd0JBQUE7RUQ2R047RUM1R007SUFBSSwrQkFBQTtZQUFBLHVCQUFBO0VEK0dWO0FBQ0Y7O0FDOUdJO0VBWko7SUFZNkIsV0FBQTtJQUFZLFlBQUE7SUFBYSxnQ0FBQTtZQUFBLHdCQUFBO0VEb0hwRDtFQ25ITTtJQUFJLCtCQUFBO1lBQUEsdUJBQUE7SUFBd0IsWUFBQTtJQUFhLGdCQUFBO0VEd0gvQztBQUNGOztBQ3RISTtFQUNJO0lBQUksaUJBQUE7RUR5SFY7QUFDRjs7QUNySEE7RUFBVSx5QkZRRztFRVJnQixVQUFBO0FEMEg3Qjs7QUN6SEk7RUFESjtJQUM2QixrQkFBQTtJQUFtQixTQUFBO0lBQVUsU0FBQTtJQUFVLFVBQUE7SUFBVyxZQUFBO0VEaUk3RTtBQUNGOztBQ2pJSTtFQUZKO0lBRTZCLGtCQUFBO0lBQW1CLFdBQUE7SUFBYSxhQUFBO0VEdUkzRDtFQ3RJTTtJQUFHLGVBQUE7RUR5SVQ7QUFDRjs7QUNySUE7RUFDSTtJQUFrQixrQkFBQTtJQUFtQixVQUFBO0lBQVcsTUFBQTtJQUFPLGFBQUE7RUQ0SXpEO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucG9pbnRlcntjdXJzb3I6IHBvaW50ZXI7fVxyXG4qe291dGxpbmU6IG5vbmUgIWltcG9ydGFudDt9XHJcblxyXG5cclxuLy8gIHRyYW5zaXRpb25cclxuJXRyYW5zaXRpb257LXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2Utb3V0Oy1tb3otdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLW91dDstby10cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2Utb3V0O3RyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1vdXQ7fVxyXG4ldHJhbnNpdGlvbjF7LXdlYmtpdC10cmFuc2l0aW9uOiAuNXMgZWFzZS1pbi1vdXQ7LW1vei10cmFuc2l0aW9uOiAuNXMgZWFzZS1pbi1vdXQ7LW8tdHJhbnNpdGlvbjogLjVzIGVhc2UtaW4tb3V0O3RyYW5zaXRpb246IC41cyBlYXNlLWluLW91dDt9XHJcbiV0cmFuc2l0aW9uMnstd2Via2l0LXRyYW5zaXRpb246IHdpZHRoIDJzIGVhc2UtaW4tb3V0Oy1tb3otdHJhbnNpdGlvbjogd2lkdGggMnMgZWFzZS1pbi1vdXQ7LW8tdHJhbnNpdGlvbjogd2lkdGggMnMgZWFzZS1pbi1vdXQ7dHJhbnNpdGlvbjogd2lkdGggMnMgZWFzZS1pbi1vdXQ7fVxyXG4ldHJhbnNpdGlvbjN7LXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMnMgZWFzZS1pbi1vdXQ7LW1vei10cmFuc2l0aW9uOiBhbGwgMnMgZWFzZS1pbi1vdXQ7LW8tdHJhbnNpdGlvbjogYWxsIDJzIGVhc2UtaW4tb3V0O3RyYW5zaXRpb246IGFsbCAycyBlYXNlLWluLW91dDt9XHJcblxyXG4vL2dyYWRpYW50XHJcbiVibGFjay1iZ3tcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBiYWNrZ3JvdW5kOiAtbW96LXJhZGlhbC1ncmFkaWVudChjZW50ZXIsIGVsbGlwc2UgY292ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogZmYzLjYrICovXHJcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWdyYWRpZW50KHJhZGlhbCwgY2VudGVyIGNlbnRlciwgMHB4LCBjZW50ZXIgY2VudGVyLCAxMDAlLCBjb2xvci1zdG9wKDQ0JSwgcmdiYSg2NSw2NCw2NiwxKSksIGNvbG9yLXN0b3AoOTUlLCByZ2JhKDQzLDQzLDQzLDEpKSwgY29sb3Itc3RvcCgxMDAlLCByZ2JhKDQxLDQxLDQxLDEpKSk7IC8qIHNhZmFyaTQrLGNocm9tZSAqL1xyXG4gICAgYmFja2dyb3VuZDotd2Via2l0LXJhZGlhbC1ncmFkaWVudChjZW50ZXIsIGVsbGlwc2UgY292ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogc2FmYXJpNS4xKyxjaHJvbWUxMCsgKi9cclxuICAgIGJhY2tncm91bmQ6IC1vLXJhZGlhbC1ncmFkaWVudChjZW50ZXIsIGVsbGlwc2UgY292ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogb3BlcmEgMTEuMTArICovXHJcbiAgICBiYWNrZ3JvdW5kOiAtbXMtcmFkaWFsLWdyYWRpZW50KGNlbnRlciwgZWxsaXBzZSBjb3ZlciwgcmdiYSg2NSw2NCw2NiwxKSA0NCUsIHJnYmEoNDMsNDMsNDMsMSkgOTUlLCByZ2JhKDQxLDQxLDQxLDEpIDEwMCUpOyAvKiBpZTEwKyAqL1xyXG4gICAgYmFja2dyb3VuZDpyYWRpYWwtZ3JhZGllbnQoZWxsaXBzZSBhdCBjZW50ZXIsIHJnYmEoNjUsNjQsNjYsMSkgNDQlLCByZ2JhKDQzLDQzLDQzLDEpIDk1JSwgcmdiYSg0MSw0MSw0MSwxKSAxMDAlKTsgLyogdzNjICovXHJcbiAgICBmaWx0ZXI6IHByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5ncmFkaWVudCggc3RhcnRDb2xvcnN0cj0nIzQxNDA0MicsIGVuZENvbG9yc3RyPScjMjkyOTI5JyxHcmFkaWVudFR5cGU9MSApOyAvKiBpZTYtOSAqL1xyXG5cclxufVxyXG5cclxuLy9mb250IHdlaWdodFxyXG4lZnctMTAwLCAuZnctMTAwe2ZvbnQtd2VpZ2h0OiAxMDA7fVxyXG4uZnctNDAwe2ZvbnQtd2VpZ2h0OiA0MDA7fVxyXG5cclxuLy9mb250LXNpemVcclxuJWZzLTE0cHh7Zm9udC1zaXplOiAxNHB4O31cclxuJWZzLTIwcHh7Zm9udC1zaXplOiAyMHB4O31cclxuXHJcbi8vIGNvbG9yXHJcbiRjb2xvci13aGl0ZTojZmZmZmZmICFpbXBvcnRhbnQ7XHJcbiRjb2xvci13aGl0ZS0zOnJnYmEoMTk0LCAxOTUsIDE5NywgMC4zKSAhaW1wb3J0YW50O1xyXG4kY29sb3Itd2hpdGUtNTpyZ2JhKDI0OCwgMjQ5LCAyNTAsIC41KSAhaW1wb3J0YW50O1xyXG4kY29sb3Itd2hpdGUtOTpyZ2JhKDI0OCwgMjQ5LCAyNTAsIC45KSAhaW1wb3J0YW50O1xyXG4kY29sb3ItYmxhY2s6IzAwMDAwMCAhaW1wb3J0YW50O1xyXG4kdGhlbWUtY29sb3ItYmxhY2s6IzJGMkYyRjtcclxuJHRoZW1lLWNvbG9yLWJsdWU6IzA0NjJFRTtcclxuXHJcbi50aGVtZS1jb2xvci1ibHVle2NvbG9yOiR0aGVtZS1jb2xvci1ibHVlO31cclxuLmJnLWJsYWNreyBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItYmxhY2s7fVxyXG5cclxuaW5wdXR7Ym9yZGVyLWNvbG9yOiRjb2xvci13aGl0ZTsgfVxyXG4uaW5wdXQtZ3JvdXAtdGV4dHtjb2xvcjogJGNvbG9yLXdoaXRlO2JhY2tncm91bmQtY29sb3I6JHRoZW1lLWNvbG9yLWJsYWNrOyBib3JkZXItY29sb3I6ICR0aGVtZS1jb2xvci1ibGFjazt9XHJcbi8vd2lkdGhzXHJcbi5tdy0xNTBweHttYXgtd2lkdGg6IDIwMHB4OyB9XHJcbi5pbnB1dC1ncm91cC1sZy01MHtcclxuICAgIEBtZWRpYSAobWluLXdpZHRoOjk5MnB4KSB7XHJcbiAgICAgICAgbWF4LXdpZHRoOiAzNSU7XHJcbiAgICB9XHJcbn1cclxuLy8gYnV0dG9uc1xyXG4uYnRuOmZvY3VzLCAuYnRuLmZvY3Vze2JveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDt9XHJcbi5idG4tcDF7cGFkZGluZzogMTFweCAyMHB4O31cclxuLmJ0bi1saWdodCB7Y29sb3I6ICRjb2xvci13aGl0ZTtiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGUtMztib3JkZXItY29sb3I6ICRjb2xvci13aGl0ZS0zO1xyXG53aWR0aDogYXV0bztsZXR0ZXItc3BhY2luZzogLjVweDtmb250LXdlaWdodDogNjAwO2hlaWdodDogNDhweDtwYWRkaW5nOiAwcHggMzBweDtcclxuICAgICY6aG92ZXIsJjpmb2N1cywgJjphY3RpdmV7YmFja2dyb3VuZC1jb2xvcjogICRjb2xvci13aGl0ZS01O2JvcmRlci1jb2xvcjogJGNvbG9yLXdoaXRlLTU7fVxyXG59XHJcbi5idG4td2hpdGUge2NvbG9yOiAkdGhlbWUtY29sb3ItYmx1ZTtiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGUtOTtib3JkZXItY29sb3I6ICRjb2xvci13aGl0ZS0zO1xyXG4gICAgd2lkdGg6IDE4MHB4O2xldHRlci1zcGFjaW5nOiAuNXB4O2ZvbnQtd2VpZ2h0OiA2MDA7aGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgICY6aG92ZXIsJjpmb2N1cywgJjphY3RpdmV7YmFja2dyb3VuZC1jb2xvcjogICRjb2xvci13aGl0ZTtib3JkZXItY29sb3I6ICRjb2xvci13aGl0ZS01O31cclxuICAgIH1cclxuLmJ0bi1yZXNwb25zaXZle1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDo5OTJweCkge1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCA1MHB4O1xyXG4gICAgfVxyXG59XHJcbi8vIC5idG4tYXBwbGUge2JhY2tncm91bmQtY29sb3I6ICRiZy1yZWQ7Ym9yZGVyLWNvbG9yOiAkYmctcmVkOyBjb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4vLyAgICAgJjpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiAkYmctcmVkLWhvdmVyOyBib3JkZXItY29sb3I6ICRiZy1yZWQtaG92ZXI7fVxyXG4vLyB9XHJcbmJvZHl7IGJhY2tncm91bmQtY29sb3I6ICR0aGVtZS1jb2xvci1ibHVlO31cclxuQG1lZGlhIChtYXgtd2lkdGg6OTkzcHgpe1xyXG4gICAgaDF7Zm9udC1zaXplOiAycmVtO31cclxufSIsIi5wb2ludGVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4qIHtcbiAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4uZnctMTAwIHtcbiAgZm9udC13ZWlnaHQ6IDEwMDtcbn1cblxuLmZ3LTQwMCB7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi50aGVtZS1jb2xvci1ibHVlIHtcbiAgY29sb3I6ICMwNDYyRUU7XG59XG5cbi5iZy1ibGFjayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQge1xuICBib3JkZXItY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbn1cblxuLmlucHV0LWdyb3VwLXRleHQge1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMkYyRjJGO1xuICBib3JkZXItY29sb3I6ICMyRjJGMkY7XG59XG5cbi5tdy0xNTBweCB7XG4gIG1heC13aWR0aDogMjAwcHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge1xuICAuaW5wdXQtZ3JvdXAtbGctNTAge1xuICAgIG1heC13aWR0aDogMzUlO1xuICB9XG59XG5cbi5idG46Zm9jdXMsIC5idG4uZm9jdXMge1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tcDEge1xuICBwYWRkaW5nOiAxMXB4IDIwcHg7XG59XG5cbi5idG4tbGlnaHQge1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDE5NCwgMTk1LCAxOTcsIDAuMykgIWltcG9ydGFudDtcbiAgd2lkdGg6IGF1dG87XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgaGVpZ2h0OiA0OHB4O1xuICBwYWRkaW5nOiAwcHggMzBweDtcbn1cbi5idG4tbGlnaHQ6aG92ZXIsIC5idG4tbGlnaHQ6Zm9jdXMsIC5idG4tbGlnaHQ6YWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjUpICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5idG4td2hpdGUge1xuICBjb2xvcjogIzA0NjJFRTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDgsIDI0OSwgMjUwLCAwLjkpICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogcmdiYSgxOTQsIDE5NSwgMTk3LCAwLjMpICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxODBweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBoZWlnaHQ6IDQ4cHg7XG59XG4uYnRuLXdoaXRlOmhvdmVyLCAuYnRuLXdoaXRlOmZvY3VzLCAuYnRuLXdoaXRlOmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDI0OCwgMjQ5LCAyNTAsIDAuNSkgIWltcG9ydGFudDtcbn1cblxuLmJ0bi1yZXNwb25zaXZlIHtcbiAgd2lkdGg6IGF1dG87XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmJ0bi1yZXNwb25zaXZlIHtcbiAgICBwYWRkaW5nOiAwcHggNTBweDtcbiAgfVxufVxuXG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA0NjJFRTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDk5M3B4KSB7XG4gIGgxIHtcbiAgICBmb250LXNpemU6IDJyZW07XG4gIH1cbn1cbi5ibGFjay1iZyB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdHJhbnNmb3JtLW9yaWdpbjogdG9wIGxlZnQ7XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkzcHgpIHtcbiAgLmJsYWNrLWJnIHtcbiAgICB3aWR0aDogNjElO1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAgdHJhbnNmb3JtOiBza2V3WCgtMzdkZWcpO1xuICB9XG4gIC5ibGFjay1iZyBpbWcge1xuICAgIHRyYW5zZm9ybTogc2tld1goMzdkZWcpO1xuICAgIGhlaWdodDogNzB2aDtcbiAgICBtYXJnaW4tbGVmdDogMjB2aDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDk5M3B4KSBhbmQgKG1pbi1oZWlnaHQ6IDgwMHB4KSB7XG4gIC5ibGFjay1iZyB7XG4gICAgdHJhbnNmb3JtOiBza2V3WCgtMjdkZWcpO1xuICB9XG4gIC5ibGFjay1iZyBpbWcge1xuICAgIHRyYW5zZm9ybTogc2tld1goMjdkZWcpO1xuICB9XG59XG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmJsYWNrLWJnIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDc1dmg7XG4gICAgdHJhbnNmb3JtOiBza2V3eSgtMTNkZWcpO1xuICB9XG4gIC5ibGFjay1iZyBpbWcge1xuICAgIHRyYW5zZm9ybTogc2tld1koMTNkZWcpO1xuICAgIGhlaWdodDogNjN2aDtcbiAgICBtYXJnaW4tdG9wOiAyOHZoO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkzcHgpIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLmJsYWNrLWJnIGltZyB7XG4gICAgbWFyZ2luLWxlZnQ6IDU2dmg7XG4gIH1cbn1cblxuLmJsdWUtYmcge1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICB6LWluZGV4OiAxO1xufVxuQG1lZGlhIChtaW4td2lkdGg6IDk5M3B4KSB7XG4gIC5ibHVlLWJnIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAzMnZoO1xuICAgIHJpZ2h0OiA3JTtcbiAgICB3aWR0aDogNDAlO1xuICAgIHotaW5kZXg6IDEwMTtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDk5M3B4KSB7XG4gIC5ibHVlLWJnIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMTVweDtcbiAgfVxuICAuYmx1ZS1iZyBoMSB7XG4gICAgZm9udC1zaXplOiAycmVtO1xuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge1xuICAuYWJzb2x1dGUtdHItYnRucyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgdG9wOiAwO1xuICAgIHotaW5kZXg6IDIwMDA7XG4gIH1cbn0iLCJAaW1wb3J0IFwiLi4vLi4vdmFyaWFibGVcIjtcclxuXHJcbi5ibGFjay1iZ3tcclxuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgdHJhbnNmb3JtLW9yaWdpbjogdG9wIGxlZnQ7XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDo5OTNweCkge3dpZHRoOiA2MSU7aGVpZ2h0OiAxMDB2aDt0cmFuc2Zvcm06IHNrZXdYKC0zN2RlZyk7XHJcbiAgICAgICAgaW1ne3RyYW5zZm9ybTogc2tld1goMzdkZWcpO2hlaWdodDogNzB2aDttYXJnaW4tbGVmdDogMjB2aDt9XHJcbiAgICB9XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDo5OTNweCkgYW5kIChtaW4taGVpZ2h0OjgwMHB4KSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBza2V3WCgtMjdkZWcpO1xyXG4gICAgICAgIGltZ3t0cmFuc2Zvcm06IHNrZXdYKDI3ZGVnKTt9XHJcbiAgICB9XHJcbiAgICBAbWVkaWEgKG1heC13aWR0aDo5OTJweCl7d2lkdGg6IDEwMCU7aGVpZ2h0OiA3NXZoO3RyYW5zZm9ybTogc2tld3koLTEzZGVnKTtcclxuICAgICAgICBpbWd7dHJhbnNmb3JtOiBza2V3WSgxM2RlZyk7aGVpZ2h0OiA2M3ZoO21hcmdpbi10b3A6IDI4dmg7fVxyXG5cclxuICAgIH1cclxuICAgIEBtZWRpYSAobWluLXdpZHRoOjk5M3B4KSBhbmQgKG1heC13aWR0aDoxMjAwcHgpe1xyXG4gICAgICAgIGltZ3ttYXJnaW4tbGVmdDogNTZ2aDt9XHJcblxyXG4gICAgfVxyXG4gICAgXHJcbn1cclxuLmJsdWUtYmcge2NvbG9yOiRjb2xvci13aGl0ZTt6LWluZGV4OiAxO1xyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6OTkzcHgpe3Bvc2l0aW9uOiBhYnNvbHV0ZTt0b3A6IDMydmg7cmlnaHQ6IDclO3dpZHRoOiA0MCU7ei1pbmRleDogMTAxO31cclxuICAgIEBtZWRpYSAobWF4LXdpZHRoOjk5M3B4KXtwb3NpdGlvbjogcmVsYXRpdmU7d2lkdGg6IDEwMCU7IHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgaDF7Zm9udC1zaXplOiAycmVtO31cclxuICAgIH1cclxuXHJcblxyXG59XHJcbkBtZWRpYSAobWluLXdpZHRoOjk5MnB4KXtcclxuICAgIC5hYnNvbHV0ZS10ci1idG5ze3Bvc2l0aW9uOiBhYnNvbHV0ZTtyaWdodDogMHB4O3RvcDogMDt6LWluZGV4OiAyMDAwO31cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../layout/layout.module */ "./src/app/layout/layout.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let HomeModule = class HomeModule {
};
HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_3__["HomeRoutingModule"], _layout_layout_module__WEBPACK_IMPORTED_MODULE_5__["LayoutModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"]
        ]
    })
], HomeModule);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map